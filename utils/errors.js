class InvalidCharacterError extends Error {
  constructor(name) {
    super(`Invalid character: ${name}`);
    this.name = 'InvalidCharacterError';
  }
}

module.exports = {
  InvalidCharacterError
}
