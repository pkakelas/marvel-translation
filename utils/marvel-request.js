const config = require('config')
const md5 = require('md5')

const getAuthParams = () => {
  const publicKey = config.get('marvelAPI.publicKey')
  const privateKey = config.get('marvelAPI.privateKey')

  const ts = Date.now()
  const hash = md5(ts + privateKey + publicKey)

  return {
    apikey: publicKey,
    ts: ts,
    hash: hash
  }
}

module.exports = {
  getAuthParams
}
