const removeIdFromComics = (comics) => {
  return comics.map(comic => {
    return {
      name: comic.name,
      description: comic.description
    }
  })
}

module.exports = {
  removeIdFromComics
}
