const config = require('config')
const log = require('./utils/logger')
const Transifex = require('./models/transifex')
const { getComicsOfCharacters } = require('./marvelAPI')
const { removeIdFromComics } = require('./utils/helpers')


const main = async () => {
  const api = new Transifex({
    projectName: config.get('transifex.project'),
    user: 'api',
    password: config.get('transifex.token'),
  })

  const characters = config.get('characters')
  const characterComics = await getComicsOfCharacters(characters)

  for (let record of characterComics) {
    const comics = record.comics
    const name = record.name

    try {
      await api.createOrUpdateResource(name, removeIdFromComics(comics))
    }
    catch (e) {
      winston.error(`Could not create or update resource ${name}`, e.message)
    }
  }
}

main().catch(e => `[TOP_LEVEL_ERROR]: ${e.message}`)
