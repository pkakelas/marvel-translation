const config = require('config')
const md5 = require('md5')
const { getAllComics } = require('./models/Comics')
const Character = require('./models/Character')
const log = require('./utils/logger')

const getComicsOfCharacters = async (names) => {
  let characters = []
  let results = []

  for (let name of names) {
    try {
      characters[name] = new Character(name)
      await characters[name].updateId()
    }
    catch (e) {
      log.warn(e.message)
    }
  }

  const comics = await getAllComics(Object.values(characters))

  for (let character of Object.values(characters)) {
    results.push({
      name: character.name,
      comics: character.getAssociatedComics(comics)
    })
  }

  return results
}

module.exports = {
  getComicsOfCharacters
}
