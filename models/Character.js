const axios = require('axios')
const log = require('../utils/logger')
const { getAuthParams } = require('../utils/marvel-request')
const { InvalidCharacterError } = require('../utils/errors')

class Character {
  constructor(name) {
    this._id = undefined
    this.name = name
  }

  setId (id) {
    this.id = id
  }

  getAssociatedComics (comics) {
    let comicsOfCharacter = []

    for (let comic of comics) {
      if (!comic.characters.includes(this.name)) {
        continue
      }

      comicsOfCharacter.push({
        id: comic.id,
        title: comic.title,
        description: comic.description
      })
    }

    return comicsOfCharacter
  }
  
  async updateId () {
    log.info(`[MARVEL_API] Fetching character ID of name: ${this.name}`)
  
    const options = {
      url: 'https://gateway.marvel.com:443/v1/public/characters',
      params: {
        name: this.name,
        ...getAuthParams()
      }
    }

    const res = await axios(options).then(res => res.data.data.results)

    if (res.length == 0) {
      throw new InvalidCharacterError(this.name)
    }

    this.setId(res[0].id)
  }

  getId () {
    return this.id
  }
}

module.exports = Character
