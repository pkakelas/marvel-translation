const axios = require('axios')
const config = require('config')
const log = require('../utils/logger')
const { getAuthParams } = require('../utils/marvel-request')

const LIMIT = config.get("comicsPagination")

const getAllComics = async (characters = []) => {
  if (characters.length == 0) {
    return [] 
  }

  const characterIds = characters.map(ch => ch.getId())
  const filterComicData = (comic) => {
    return {
      id: comic.id,
      title: comic.title,
      description: comic.description,
      characters: comic.characters.items.map(ch => ch.name)
    }
  }

  let round = 0
  let comics = []
  let res

  log.info(`[MARVEL_API] Getting all character comics`)
  do {
    const offset = round * LIMIT

    log.info(`[MARVEL_API] Fetching batch (${offset}, ${offset + LIMIT})`)
    const options = {
      url: 'https://gateway.marvel.com:443/v1/public/comics',
      params: {
        characters: characterIds.join(','),
        limit: LIMIT,
        offset: offset,
        ...getAuthParams()
      }
    }
  
    res = await axios(options).then(res => res.data.data.results.map(filterComicData))

    comics = comics.concat(res)
    ++round
  } while (res.length == LIMIT)
  
  return comics
}

module.exports = {
  getAllComics
}
