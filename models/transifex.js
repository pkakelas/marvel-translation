const TransifexBase = require('transifex-api-es6')
const log = require('../utils/logger')

class Transifex extends TransifexBase {
  // TODO: Make sure it's working and harden it
  async updateResource (name, content) {
    return this._send(`/resource/${name}/content`, {
      method: 'PUT',
      json: true,
      body: content
    })
  }

  // TODO: Error handling
  async createResource (name, content) {
    return super.createResource({
      name: name,
      slug: name,
      i18n_type: 'KEYVALUEJSON',
      content: JSON.stringify(content)
    })
  }

  async resourceExists (name)  {
    const resources = await this.getResources()
    const names = resources.map(resource => resource.name)
    
    return names.includes(name)
  }

  async createOrUpdateResource (name, content) {
    if (await this.resourceExists(name))  {
      log.info(`Resource exists. Updating resource.`)
      return this.updateResource(name, content) 
    }

    log.info(`Resource doesn't exist. Creating resource.`)
    return this.createResource(name, content)
  }
}

module.exports = Transifex
